$(window).bind("load", function() {
    //$('.pre-loader').fadeOut();
    $('.pre-loader').addClass('active');
});

$(document).ready(function() {
	$('.menu-bar').click(function() {
		$('.menu-sec').toggleClass('active');
	});

	$('.colse-btn, .menu-sec li a').click(function() {
		$('.menu-sec').removeClass('active');
	});

	$('.expand-lnk').click(function() {
		$(this).toggleClass('active');
		$('.expan-hidden').slideToggle('slow');
	});

});

$(window).scroll(function(){

    $('section').each(function() {
        if($(window).scrollTop() >= $(this).offset().top + $(this).height() - $(window).height() / 2 || $(window).scrollTop() < $(this).offset().top - $(window).height() / 2) 
            $(this).removeClass('');
        else
            $(this).addClass('active');
    });

});

$('.mn-nav li a').click( function () {
	$('.mn-nav li').removeClass('active');
	$(this).parents('li').addClass('active');
	$('body').css('overflow-y','auto');
	var tab = $(this).attr('data-tab');
	
	$('.page-sec').hide();
	$('.page-sec[data-tab-no='+tab+']').fadeIn();

	var sVal = $('.page-sec[data-tab-no='+tab+']').offset().top - 40;
   	$('html,body').animate({
    	scrollTop: sVal
   	},'slow');
});

$('.scroll-icon').click( function () {
	$('body').css('overflow-y','auto');
	$('.page-sec').hide();
	$('.page-sec[data-tab-no=1]').fadeIn();
	var sVal = $('.page-sec').offset().top - 40;
   	$('html,body').animate({
    	scrollTop: sVal
	},'slow');
});

$(window).scroll(function() {
	fixHeader();	
});

fixHeader();

function fixHeader() {
	var po = $('.btm-line').offset().top - 160;
	var bo = $('html').scrollTop();
	if(bo>po) {
		$('.bt-nav').addClass('fixed');
	}
	else
	{
		$('.bt-nav').removeClass('fixed');	
	}
}